// package com.inchequer

import scala.collection.mutable
import scala.collection.immutable
import scala.xml

// TODO: Make a date class to handle date comparisons and the recurrences
// TODO: Emplement date arithmetic (e.g. + 1)
// TODO: Don't store strings for dates anywhere

/**
 * The Account stores a list of records
 * of transactions for an account.
 */

class Account {
  /** All (active) records in an account, indexed by their id.
   *  When unscheduled entries are generated from a recurrence,
   *  they are also inserted here.
   *  The id of unscheduled entries is taken from the scheduled
   *  date and the recurrence id. Therefore, if an entry is
   *  scheduled it will be saved and not regenerated again
   *  (since the id is not changed).
   */
  var list: mutable.Map[String,Record] = new mutable.HashMap[String,Record]()
  /** The last number used in a generated id */
  var lastId: Int = 0

  /** Constructor for account that reads an xml file */

  def this(file: String) = {
    this()
 	val account = xml.XML.loadFile(file)
 	// Process the account entry and all of its records
	account match {
	  case <account>{entries @ _*}</account> => {
	    lastId = (account \ "@lastid").text.toInt
	    for (entry <- entries if Record.knownRecordXml(entry)) {
	      val r = Record.loadXml(entry)
	      list += r.id -> r
	    }
	  }
	}
    
    for ((id, rec) <- list if canPrune(rec)) list -= id
  }

  /**
   * A record must be saved.
   */

  def mustSave(r: Record) : Boolean = {
    r match {
      case x: Entry => if (! x.scheduled) return false
      case _ =>
    }
    return true
  }

  /**
   * A record that was read from xml can be ignored
   * because it's no longer visible.
   */

  def canPrune(r: Record): Boolean = {
    if (r.deleted) return true
    r match {
      case x: Entry => if (x.cleared) return true
      case x: Recurrence => if (x.endDate > "" && x.endDate <= x.date) return true
      case x: Balance => if (x.date < lastBalance) return true
    }
    return false
  }

  /** Return a record given it's id */

  def get(idx: String): Record = {
    list(idx)
  }

  /** Return the next available number for use in a unique id */

  def nextId(t: String): String = {
    lastId += 1
    t + "%07d".format(lastId)
  }

  /** Return xml of all records */ 
  
  def toXml: xml.Elem = <account lastid="0">{
      for (r <- list.toList.sortBy(_._1) if mustSave(r._2)) yield r._2.toXml
    }</account>

  /** Return the date of the last balance in the account */

  def lastBalance: String = {
    var date = ""
    for ((id, b: Balance) <- list) {
      if (b.date > date) date = b.date
    }
    date
  }
}

/** Account object, providing main */

object Account {
  def main(args: Array[String]) {
    // Read account
	val a = new Account("conf/test.xml")
	
	// Add recurrences...
	// Don't modify list while scanning it!
	for (x <- a.list.values)
	  x match {
	    case r: Recurrence => {
	      for (e <- r.gen())
			if (! a.list.contains(e.id)) 
		        a.list += e.id -> e
	    }
	    case _ =>
	  }

	println(new xml.PrettyPrinter(80, 2).format(a.toXml))
  }


}

