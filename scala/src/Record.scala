// package com.inchequer

import scala.collection.mutable
import scala.collection.immutable
import scala.xml.XML
import java.text.ParseException
import com.google.ical.iter.RecurrenceIteratorFactory
import com.google.ical.util.DTBuilder
import java.util.TimeZone

/**
 * Accounts are made up of records
 * which can be serialized. This superclass
 * alone cannot be serialized or constructed.
 */

abstract class Record {
  /** A name to display */
  var name: String = ""
  /** A unique identifier */
  var id: String = ""
  /** The record was edited */
  var dirty: Boolean = false
  /** The date of transaction, or start date of a recurrence */
  var date: String = ""
  /** Amount of transaction (positive or negative???) */
  var amount: Int = 0
  /** The entry was deleted */
  var deleted: Boolean = false;

  /** Modify a record with values given in a map */

  def loadMap(m: Map[String, String]) = {
    for (x <- m) x match {
      case ("id", y) => id = y
      case ("name", y) => name = y
      case ("date", y) => date = y
      case ("amount", y) => amount = y.toInt
      case _ =>
    }
  }

  /** Return a map of values */

  def toMap = {
    immutable.TreeMap("id" -> id, "name" -> name, "date" -> date, "amount" -> amount.toString)
  }

  /** Modify a record with values given in xml */

  def loadXml(node: xml.Node) = {
    id = (node \ "id").text
    name = (node \ "name").text
    date = (node \ "date").text
    amount = (node \ "amount").text.toInt
  }

  /** Helper method for serializing subclasses to xml */

  protected def toNodeBuffer = {
    val n = new xml.NodeBuffer
    n += <id>{id}</id>
    n += <name>{name}</name>
    n += <date>{date}</date>
    n += <amount>{amount}</amount>
    n
  }

  /** Return an xml of values */

  def toXml: xml.Elem

  /** Convert to string, for debugging */

  override def toString = this.getClass.getSimpleName + ": " + name
}

/**
 * The Record object is provided to give a factor method
 * for creating instances of subclasses.
 */

object Record {

  /** Given some xml, decide if it's a known Record subclass. */

  def knownRecordXml(node: xml.Node): Boolean = {
    (node.label == "entry") || (node.label == "balance") || (node.label == "recurrence")
  }

  /**
   * Factory method that returns the particular
   * Record instance defined by xml. Should only
   * be called if knwonRecordXml returns true.
   */

  def loadXml(node: xml.Node): Record = {
    if (node.label == "balance") new Balance(node)
    else if (node.label == "entry") new Entry(node)
    else if (node.label == "recurrence") new Recurrence(node)
    else throw new ParseException(node.label + " not a valid entry", 0)
  }
}

/**
 * The Entry is a Record that contains
 * amounts for credit or debit to an account
 * with a name and date.
 */

// Add more fields
// Add methods for updating entry
// Do we just agree that a negative amount is a credit?
// Or should we reverse the sign +credit -debit

class Entry extends Record {
  /** The entry was cleared, that is, posted to the bank account */
  var cleared: Boolean = false;
  /** The entry was scheduled, that is, payment committed */
  var scheduled: Boolean = false;

  /** Constructor from values in a map */

  def this(m: Map[String, String]) = {
    this()
    loadMap(m)
  }

  /** Modify a record with values given in a map */

  override def loadMap(m: Map[String, String]) = {
    super.loadMap(m)
    for (x <- m) x match {
      case ("cleared", y) => cleared = y.toBoolean
      case ("scheduled", y) => scheduled = y.toBoolean
      case _ =>
    }
  }

  /** Return a map of values */

  override def toMap() = {
    var m = super.toMap
    m += ("cleared" -> cleared.toString)
    m += ("scheduled" -> scheduled.toString)
    m
  }

  /** Constructor for xml */

  def this(node: xml.Node) = {
    this()
    loadXml(node)
  }

  /** Modify a record with values given in xml */

  override def loadXml(node: xml.Node) = {
    super.loadXml(node)
    cleared = (node \ "cleared").text.toBoolean
    scheduled = (node \ "scheduled").text.toBoolean
  }

  /** Helper method for serializing to xml */

  override protected def toNodeBuffer = {
    val n = super.toNodeBuffer
    n += <cleared>{cleared}</cleared>
    n += <scheduled>{scheduled}</scheduled>
    n
  }

  /** Return an xml of values */

  override def toXml = <entry>{toNodeBuffer}</entry>

  /** Convert to string, for debugging */

  override def toString = s"Entry: ($id) $name on $date amount $amount"
}

/**
 * The Balance is a Record that contains
 * an account balance on a given date.
 */

class Balance extends Record {

  /** Constructor from values in a map */

  def this(m: Map[String, String]) = {
    this()
    loadMap(m)
  }

  /** Modify a record with values given in a map */

  override def loadMap(m: Map[String, String]) = {
    super.loadMap(m)
    name = "balance"
  }

  /** Return a map of values */

  override def toMap() = {
    var m = super.toMap
    m += ("name" -> "balance")
    m
  }

  /** Constructor for xml */

  def this(node: xml.Node) = {
    this()
    name = "balance"
    loadXml(node)
  }
  
  /** Modify a record with values given in xml */

  override def loadXml(node: xml.Node) = {
    super.loadXml(node)
    name = "balance"
  }

  /** Return an xml of values */

  override def toXml = <balance>{toNodeBuffer}</balance>

  /** Convert to string, for debugging */

  override def toString = s"Balance: ($id) on $date amount $amount"
}

/**
 * The Recurrence is a Record that contains
 * amounts for credit or debit to an account
 * with a name and a recurrence rule.
 * The date given to a recurrence is the start
 * date, or more accurately, the day before the
 * first available date of the recurrence.
 */

class Recurrence extends Record {
  /** Recurrence using RFC 2445 specification */
  var recur: String = ""
  /** Optional end date (recurrences can't be deleted) */
  var endDate: String = "";

  /** Constructor from values in a map */

  def this(m: Map[String, String]) = {
    this()
    loadMap(m)
  }

  /** Modify a record with values given in a map */

  override def loadMap(m: Map[String, String]) = {
    super.loadMap(m)
    for (x <- m) x match {
      case ("recur", y) => recur = y
      case ("endDate", y) => endDate = y
      case _ =>
    }
  }

  /** Return a map of values */

   override def toMap() = {
    var m = super.toMap
    m += ("recur" -> recur)
    m += ("endDate" -> endDate)
    m
  }

  /** Constructor for xml */

  def this(node: xml.Node) = {
    this()
    loadXml(node)
  }

  /** Modify a record with values given in xml */

  override def loadXml(node: xml.Node) = {
    super.loadXml(node)
    recur = (node \ "recur").text
  }

  /** Helper method for serializing to xml */

  override protected def toNodeBuffer = {
    val n = super.toNodeBuffer
    n += <recur>{recur}</recur>
    n
  }

  /** Return an xml of values */

  override def toXml = <recurrence>{toNodeBuffer}</recurrence>

  /** Generate 5 entries, this is a prototype only. */

  def gen() = {
    // Get base ident
    val base = id.split(":")(1)

    val dtStart = new DTBuilder(2014, 1, 1).toDate()

	// RecurrenceIteratorFactory.createRecurrenceIterator(java.lang.String rdata, DateValue dtStart, java.util.TimeZone tzid, boolean strict)
    // Put the date in EXDATE to exclude it... this eliminates some
	// extra logic, but it requires that the user specifies a start
	// date before the first date in the recurrence... usually
	// this is not a problem, but it could be more challenging
	// when ending a recurrence on one day and starting it the next
	// We could also require the user to include their own EXDATE

	val r = RecurrenceIteratorFactory.createRecurrenceIterator(
	    recur + "\nEXDATE:" + date,
	    dtStart,
	    TimeZone.getDefault(),
	    true)
	var i = 0
	val buff = new mutable.ListBuffer[Entry]
	while (r.hasNext() && i < 5) {
	  val n = r.next.toString
	  i += 1
	  val e = new Entry(Map("id" -> (n+":"+base), "name" -> name, "date" -> n, "amount" -> amount.toString))
	  buff += e
	}
    buff.toList
  }

  /** Convert to string, for debugging */

  override def toString = s"Recurrence: ($id) $name starting $date amount $amount recurs $recur"
}
